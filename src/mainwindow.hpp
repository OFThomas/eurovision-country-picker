#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>

#include "random_numbers.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(std::vector<std::string> text_list, QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void on_button_clicked();

private slots:
//    void on_graphicsView_rubberBandChanged(const QRect &viewportRect, const QPointF &fromScenePoint, const QPointF &toScenePoint);

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QGraphicsEllipseItem *ellipse;

    std::vector<std::string> options;
    Random_number rand;
    void UpdateLabel(QString text);
};

#endif // MAINWINDOW_HPP
