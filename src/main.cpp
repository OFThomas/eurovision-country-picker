#include "mainwindow.hpp"
#include <QApplication>

#include <fstream>
#include <iostream>

int main(int argc, char *argv[])
{
    /*
    // read in the country list
    // pass to window
    std::vector<std::string> countries;

    std::ifstream country_file("countries.txt");

    std::string line;
    while(std::getline(country_file, line))
    {
        countries.push_back(line);
    }
*/
    std::vector<std::string> countries{ "Israel", "UK", "Germany", "France", "Spain",
                                      "Italy", "Greece", "Belarus", "Serbia", "Cyprus",
                                      "Estonia", "Czech Republic", "Australia", "Iceland", "San Marino",
                                        "Slovenia", "North Macedonia", "The Netherlands", "Albania", "Sweden",
                                        "Russia", "Azerbaijan", "Denmark", "Norway", "Switzerland",
                                        "Malta"};

    QApplication a(argc, argv);
    // pass countries to the window
    MainWindow w(countries);
    w.show();

    return a.exec();
}
