#ifndef RANDOM_NUMBERS_HPP
#define RANDOM_NUMBERS_HPP

#include <random>
#include <chrono>

class Random_number
{
    private:
        int min = 0;
        int max = 1;

        unsigned seed;

        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution;

    public:

        Random_number(int max_val=1) : max(max_val)
    {
        seed = std::chrono::system_clock::now().time_since_epoch().count();

        generator.seed(seed);
            
        // change distribution to be between 0 & max
        distribution = std::uniform_int_distribution<int>(0,max);
    }

        int get()
        {
            return distribution(generator);
        }
};

#endif // RANDOM_NUMBERS_HPP
