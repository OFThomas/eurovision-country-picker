#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include "random_numbers.hpp"
#include <iostream>
#include <chrono>
#include <thread>

MainWindow::MainWindow(std::vector<std::string> text_list, QWidget *parent) :
    options(text_list),
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui -> pushButton, SIGNAL(clicked(bool)), this, SLOT(on_button_clicked()));

    rand = Random_number((int)options.size() - 1);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_button_clicked()
{
    int shuffle_size = 10;
    for(int i = 0; i < shuffle_size; i++)
    {
    //static int pos = 0;
    int pos = rand.get();

    if(pos == 26) std::cout << "err pos = 26" << std::endl;

    if(i == shuffle_size - 1)
    std::cout << pos << ". " << options[pos] << std::endl;

    // stupid cast wants a Qstring so pass a reference to the first char of the string
    ui -> label -> setText(&options[pos][0]);
    // pos++;

    // do graphics
    QGraphicsScene *scene = new QGraphicsScene();

    // get picture location
    // std::string loc = "/usr/local/flags/" + options[pos] + ".svg";
    std::string loc = "../flags/" + options[pos] + ".svg";
    QPixmap m = QPixmap(&loc[0]);

    QGraphicsPixmapItem * item = scene -> addPixmap(m);
    ui -> graphicsView -> setScene(scene);
    ui -> graphicsView -> fitInView(item); //, Qt::KeepAspectRatio);

    QCoreApplication::processEvents();

    // sleep for half a second
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
}
